import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import {Track} from "./track.schema";
import * as mongoose from "mongoose";

export type CommentDocument = Comment & Document;

@Schema()
export class Comment {
    @Prop()
    text: string;

    @Prop()
    username: string;

    @Prop({type: mongoose.Schema.Types.ObjectId, ref: 'Track'})
    trackId: Track;
}

export const CommentSchema = SchemaFactory.createForClass(Comment);