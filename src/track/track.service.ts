import {Injectable} from "@nestjs/common";
import {InjectModel} from "@nestjs/mongoose";
import {Track, TrackDocument} from "./schemas/track.schema";
import {Model, ObjectId} from "mongoose";
import {Comment, CommentDocument} from "./schemas/comment.schema";
import {CreateTrackDto} from "./dto/create-track.dto";
import {CreateCommentDto} from "./dto/create-comment.dto";
import {FileService, FileType} from "../file/file.service";

@Injectable()
export class TrackService {
    constructor(
        @InjectModel(Track.name) private trackModel: Model<TrackDocument>,
        @InjectModel(Comment.name) private commentModel: Model<CommentDocument>,
        private fileService: FileService,
    ) {
    }
    async create(dto: CreateTrackDto, picture: any, audio: any): Promise<Track> {
        const audioPath = this.fileService.createFile(FileType.AUDIO, audio[0]);
        const picturePath = this.fileService.createFile(FileType.IMAGE, picture[0]);

        return this.trackModel.create({
            ...dto,
            audio: audioPath,
            picture: picturePath,
            listens: 0,
        });
    }

    async getAll(count = 10, offset = 0): Promise<Track[]> {
        return this.trackModel.find().skip(offset).limit(count);
    }

    async getOne(id: ObjectId): Promise<Track> {
        return this.trackModel.findById(id).populate('comments');
    }

    async delete(id: ObjectId): Promise<ObjectId> {
        const track = await this.trackModel.findByIdAndDelete(id)
        return track.id
    }

    async addComment(dto: CreateCommentDto): Promise<Comment> {
        const track = await this.trackModel.findById(dto.trackId)
        const comment = await this.commentModel.create({...dto})
        track.comments.push(comment.id)
        await track.save()
        return comment
    }

    async  listen(id: ObjectId) {
        const track = await this.trackModel.findById(id)
        track.listens += 1
        track.save()
    }

    async search(query: string): Promise<Track[]> {
        return this.trackModel.find({
            name: {$regex: new RegExp(query, 'i')}
        })
    }
}